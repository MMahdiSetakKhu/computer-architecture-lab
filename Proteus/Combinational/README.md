# COMBINATIONAL CIRCUITS

In this homework, we designed, constructed, and tested four combinational logic circuits.
**Design Example** and **Majority Logic** circuits are constructed with NAND gates,
**Parity Generator** with XOR gates,
**Decoder Implementation** with a decoder and NAND gates.