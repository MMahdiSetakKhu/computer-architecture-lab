# Majority Logic

A majority logic is a digital circuit whose output is equal to 1 if the majority of the inputs
are 1’s. The output is 0 otherwise. Design and test a three-input majority circuit using
NAND gates with a minimum number of ICs

# Solution

![](./Truth%20Table.png)
![](./Karnaugh%20Map.png)
![](./Solution.jpg)

# Circuit

![](./Circuit.png)
