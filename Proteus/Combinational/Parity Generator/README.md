# Parity Generator

Design, construct, and test a circuit that generates an even parity bit from four message
bits. Use XOR gates. Adding one more XOR gate, expand the circuit so that it generates
an odd parity bit also.

# Circuits

![](./Circuit.png)
![](./CircuitV2.png)
