module Datapath();
	reg [63:0] Address;
	wire Clock, ALUSrc, MemToReg, RegWrite, MemRead, MemWrite, Branch, Zero;
	wire [1:0] ALUOp;
	wire [31:0] Instruction;
	wire [63:0] ReadData1, ReadData2, WriteData, ImmOut, ALUSecond, ALUres, ReadData, PCUpdate;
	wire [3:0] ALUctrl;

	clock_generator clk(Clock);
	InstructionMemory inst_mem(Address, Instruction);
	control ctrl(Instruction[6:0], ALUSrc, MemToReg, RegWrite, MemRead, MemWrite, Branch, ALUOp);
	RegisterFile reg_file(Clock, Instruction[19:15], Instruction[24:20], ReadData1, ReadData2, RegWrite, Instruction[11:7], WriteData);
	ImmGen imm_gen(Instruction, ImmOut);
	ALUControl alu_ctrl(ALUOp, Instruction[31:25], Instruction[14:12], ALUctrl);
	Mux alu_mux(ReadData2, ImmOut, ALUSrc, ALUSecond);
	DataMemory data_mem(Clock, ALUres, MemRead, ReadData, MemWrite, WriteData);
	ALU alu(ReadData1, ALUSecond, ALUctrl, ALUres, Zero);
	Mux reg_mux(Address+4, ImmOut, MemToReg, WriteData);

	Mux pc_mux(ALUres, ReadData, Zero&Branch, PCUpdate);
	always @(posedge Clock)
		Address <= PCUpdate;
endmodule
