module PC_test_bench();

	reg Clock, enable, reset, load, dir, leftInput, rightInput;
	reg [63:0] in;
	wire [63:0] reg_out, adder_out;

	initial begin
		Clock = 0;
		forever #5 Clock = ~Clock;
	end

	CLAFullAdder64b adder(.Cin(0), .A(reg_out), .B({0, 4}), .Sum(adder_out));
	ShiftRegister shReg(.Enable(enable), .Reset(reset), .Load(load), .Input(adder_out), .Dir(dir), .LeftInput(leftInput), .RightInput(rightInput), .Clock(Clock), .Q(reg_out));
	defparam shReg.n = 64;

	initial begin
		enable = 0;
		reset = 0;
		load = 1;
		dir = 1;
		leftInput = 1;
		rightInput = 1;
		#20 $display("initial register's bits: %b", reg_out);
		reset = 1;
		#20 $display("register's bits after reset: %b", reg_out);
		reset = 0;
		$display("start counting:");
		$display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
		#20 $display("reg: %d", reg_out);
	end

endmodule