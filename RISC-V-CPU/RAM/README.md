# RAM
Simple 64 bits RAM initialized with its own index.
## RAM Test Bench Result
```
# Address:   2 value:                    2
# Address: 111 value:                  111
# Address:  53 value:                   53
# Address:  21 value:                   21
# Address:   7 value:                    7
# Address:  65 value:                   65
# Address:  36 value:                   36
```